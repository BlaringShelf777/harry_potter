import "./styles.css"

export default function Menu({ active: { cards, my_chars }, setActive }) {
	const handle_newCard = () => {
		setActive({ cards: true, my_chars: false })
	}

	const handle_myChars = () => {
		setActive({ cards: false, my_chars: true })
	}

	return (
		<nav className="menu">
			<ul className="menu__list">
				<li
					className={`list__item${cards ? "--selected" : ""}`}
					onClick={handle_newCard}
				>
					Get new characters
				</li>
				<li
					className={`list__item${my_chars ? "--selected" : ""}`}
					onClick={handle_myChars}
				>
					My characters
				</li>
			</ul>
		</nav>
	)
}
