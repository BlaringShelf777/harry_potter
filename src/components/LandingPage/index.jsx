import "./styles.css"

export default function LandingPage({ rand_char_arr, setNextPage }) {
	return (
		<div className="initial-container">
			<div className="initial">
				<h1 className="initial__title">Tornei tribruxo</h1>
				<p className="initial__info">
					Clique no botão para encontrar os feiticeiros!
				</p>
				<button className="initial__button" onClick={rand_char_arr}>
					start
				</button>
			</div>
		</div>
	)
}
