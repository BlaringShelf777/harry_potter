import Card from "../Card"
import "./styles.css"

export default function MyChars({ my_chars }) {
	return (
		<section className="my_chars">
			{my_chars.map((char, i) => (
				<Card char={char} key={i} />
			))}
		</section>
	)
}
