import { useState } from "react"
import CardArea from "../CardArea"
import Menu from "../Menu"
import MyChars from "../MyChars"

export default function Main({ my_chars, rand_chars, randomize }) {
	const [active, setActive] = useState({ cards: true, my_chars: false })

	return (
		<>
			<Menu active={active} setActive={setActive} />
			{active.cards && (
				<CardArea rand_chars={rand_chars} randomize={randomize} />
			)}
			{active.my_chars && <MyChars my_chars={my_chars} />}
		</>
	)
}
