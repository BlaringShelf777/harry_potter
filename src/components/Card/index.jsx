import "./styles.css"

export default function Card({ char: { name, species, house, alive, image } }) {
	return (
		<div
			className={`char ${house.length ? `char--${house}` : ""}`}
			style={{ backgroundImage: `url(${image})` }}
		>
			<div className="container">
				<div className="char__info">
					<p className="info__name">{name}</p>
					<p className="info__status">
						<span
							className={`status__curr ${
								alive ? "status__curr--alive" : "status__curr--dead"
							}`}
						></span>
						{species}
					</p>
				</div>
				{house && <p className="char__house">{house}</p>}
			</div>
		</div>
	)
}
