import Card from "../Card"
import "./styles.css"

export default function CardArea({ rand_chars, randomize }) {
	return (
		<div className="rand_chars">
			<div className="rand_chars__area">
				{rand_chars.map((c, i) => (
					<Card key={i} char={c} />
				))}
			</div>
			<button className="rand_chars__more" onClick={randomize}>
				try again
			</button>
		</div>
	)
}
