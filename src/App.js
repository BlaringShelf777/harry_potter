import "./App.css"
import { useState, useEffect } from "react"
import LandingPage from "./components/LandingPage"
import Main from "./components/Main"

function App() {
	const [chars, setChars] = useState([])
	const [randChars, setRandChars] = useState([])
	const [nextPage, setNextPage] = useState(false)
	const [myChars, setMyChars] = useState([])

	const rand = () => Math.floor(Math.random() * chars.length)
	const rand_char_arr = () => {
		let arr = []
		let houses = []

		while (arr.length !== 3) {
			const char = chars[rand()]

			if (!arr.includes(char) && !houses.includes(char.house)) {
				arr.push(char)
				houses.push(char.house)
			}
		}

		const new_chars = arr.filter((char) => !myChars.includes(char))

		setRandChars(arr)
		setNextPage(true)
		setMyChars([...myChars, ...new_chars])
	}

	useEffect(() => {
		fetch("https://hp-api.herokuapp.com/api/characters")
			.then((resp) => resp.json())
			.then((resp) => setChars(resp))
			.catch((err) => console.log(err))
	}, [])

	return (
		<div className="App">
			<div className="App-header">
				{!nextPage && (
					<LandingPage
						rand_char_arr={rand_char_arr}
						setNextPage={setNextPage}
					/>
				)}
				{nextPage && (
					<Main
						my_chars={myChars}
						rand_chars={randChars}
						randomize={rand_char_arr}
					/>
				)}
			</div>
		</div>
	)
}

export default App
